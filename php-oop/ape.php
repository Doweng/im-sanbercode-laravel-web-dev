<?php

require_once('animal.php');

class Kera extends Animal {
    // public $yell= "Auuoooo";   //Jika menggunakan property
    // public $kaki= 2;

    public function yell(){
        echo "Yell : Auuoooo";
    }
                                    //Jika menggunakan fungsi
    public function kaki(){
        echo "Jumlah kaki : 2";
    }
}

?>