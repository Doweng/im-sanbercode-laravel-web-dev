<?php
echo "<h3>Output akhir:</h3>";

require_once ('animal.php');
require_once ('frog.php');
require_once ('ape.php');

$sheep= new Animal("shaun");

echo "Nama animal : $sheep->name <br>";
echo "Jumlah kaki : $sheep->legs <br>";
echo "Berdarah dingin : $sheep->cold_blooded <br><br>"; 

$kodok= new Frog("Buduk");

echo "Nama : $kodok->name <br>";
echo "Jumlah kaki : $kodok->legs <br>";
echo "Berdarah dingin : $kodok->cold_blooded <br>";
// echo "Jump : $kodok->jump<br><br>";  //Jika menggunakan property 
echo $kodok->jump() . "<br><br>"; //Jika mengunakan fungsi

$sungokong= new Kera("Kera sakti");

echo "Nama : $sungokong->name <br>";
// echo "Jumlah kaki : $sungokong->kaki <br>"; //Jika menggunakan property
echo $sungokong->kaki() . "<br>"; //Jika menggunakan fungsi
echo "Berdarah dingin : $sungokong->cold_blooded <br>";
// echo "Yell : $sungokong->yell"; //Jika menggunakan property
echo $sungokong->yell(); //Jika menggunakan fungsi

?>