<?php 

require_once('animal.php');

class Frog extends Animal {
    // public $jump= "Hop Hop";  //Jika menggunakan property

    public function jump() {
       echo "Jump : Hop Hop";   //Jika menggunakan fungsi
    }
}

?>