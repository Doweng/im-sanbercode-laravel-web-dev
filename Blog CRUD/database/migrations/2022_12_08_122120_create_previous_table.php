<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreviousTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('previous', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('cast_id'); //untuk menyesuaikan dengan kolom parent yg akan jdi foreignkey
            $table->foreign('cast_id')->references('id')->on('cast');
            $table->unsignedBigInteger('film_id');
            $table->foreign('film_id')->references('id')->on('film');

            $table->string('judul_film');
            $table->string('peran');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('previous');
    }
}
