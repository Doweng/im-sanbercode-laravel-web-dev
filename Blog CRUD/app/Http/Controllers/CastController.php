<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.tambah'); //function untuk nampilin tambah.blade.php,
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required',      //name,age,bio sesuaikn dengan yg di tabel DB Cast
            'age' => 'required',       //untuk validasi data
            'bio' => 'required',
        ],
        [
            'name.required' => 'cast name harus di isi tidak boleh kosong',
            'age.required' => 'age harus di isi tidak boleh kosong',  //validation untuk pesan error jika ada kolom yang tidak di isi
            'bio.required' => 'bio harus di isi tidak boleh kosong',
        ]);

        DB::table('cast')->insert([
            'name' => $request['name'],
            'age' => $request['age'],     //query builder untuk insert data ke database
            'bio' => $request['bio']
        ]);

        return redirect('/cast');  //untuk melempar ke halaman lain sesuai url yg di tuju get(/cast)
    }

    public function index(){
        $cast = DB::table('cast')->get(); //get ambil smua data nya

        return view('cast.tampil', ['cast' => $cast]); //tampilan di view tambah.blade & parsing dataNya
    }

    public function show($id){     //paramter $id dari route:get cast/{id},
        $cast = DB::table('cast')->find($id);

        return view('cast.detail', ['cast' => $cast]);  //untuk menampilkan detail by ID & [parsing dataNya]
    }

    public function edit($id){
        $cast = DB::table('cast')->find($id);

        return view('cast.edit', ['cast' => $cast]); 
    }

    public function update($id, Request $request){ //Request $request dari blade
        $request->validate([
            'name' => 'required',      //name,age,bio sesuaikn dengan yg di tabel DB Cast
            'age' => 'required',       //untuk validasi data
            'bio' => 'required',
        ],
        [
            'name.required' => 'cast name harus di isi tidak boleh kosong',
            'age.required' => 'age harus di isi tidak boleh kosong',  //validation untuk pesan error jika ada kolom yang tidak di isi
            'bio.required' => 'bio harus di isi tidak boleh kosong',
        ]);

        DB::table('cast')
              ->where('id', $id)
              ->update( [
                'name' => $request['name'],
                'age' => $request['age'],
                'bio' => $request['bio']
            ]);

            return redirect('/cast');
    }

    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
