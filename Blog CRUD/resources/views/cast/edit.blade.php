@extends('layout.master')

@section('judul')
    Halaman Edit Cast
@endsection

@section('content')
<form action="/cast/{{ $cast->id }}" method="POST">
@method('put')
@csrf
    <div class="form-group">
        <label>Cast Name</label>
        <input type="text" class="form-control @error('name') is-invalid @enderror" value="{{ $cast->name }}" name="name" >
    </div>
    {{-- @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror --}}
    <div class="form-group">
        <label>Age</label>
        <input type="text" class="form-control @error('age') is-invalid @enderror" value="{{ $cast->age }}" name="age">
    </div>
    {{-- @error('age')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror --}}
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control @error('bio') is-invalid @enderror">{{ $cast->bio }}</textarea>
    </div>
    {{-- @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror --}}
    <button type="submit" class="btn btn-success">Submit</button>
  </form>

  {{-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error) /.foreach untuk menampilkan error semua
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif --}}
@endsection