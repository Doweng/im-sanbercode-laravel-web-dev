@extends('layout.master')

@section('judul')
    Halaman Tambah Cast
@endsection

@section('content')
<form action="/cast" method="POST">
@csrf
    <div class="form-group">
        <label>Cast Name</label>
        <input type="text" placeholder="Masukan cast name" class="form-control @error('name') is-invalid @enderror" name="name" >
    </div>
    {{-- @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror --}}
    <div class="form-group">
        <label>Age</label>
        <input type="text" placeholder="Masukan umur" class="form-control @error('age') is-invalid @enderror" name="age">
    </div>
    {{-- @error('age')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror --}}
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" placeholder="Isikan keterang bio" class="form-control @error('bio') is-invalid @enderror"></textarea>
    </div>
    {{-- @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror --}}
    <button type="submit" class="btn btn-success">Submit</button>
  </form>

  {{-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error) /.foreach untuk menampilkan error semua
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif --}}
@endsection