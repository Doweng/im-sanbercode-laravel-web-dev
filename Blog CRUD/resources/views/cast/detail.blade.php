@extends('layout.master')

@section('judul')
    Halaman Detail Cast
@endsection

@section('content')

    <h1>Name: {{ $cast->name }}</h1>
    <h3>Age: {{ $cast->age }}</h3>
    <p>Bio: {{ $cast->bio }}</p>

    <a href="/cast" class="btn btn-secondary btn-sm">Back</a>

@endsection