@extends('layout.master')

@section('judul')
    Halaman Tampilan Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-outline-primary my-3">+ Tambah Data</a>

<table class="table table-dark">
    <thead>
      <tr>
        <th scope="col">id</th>
        <th scope="col">Name</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ( $cast as $key => $item )
          <tr>
            <td>{{ $key + 1 }}</td> 
            <td>{{ $item->name }}</td>
            <td>
                <form action="/cast/{{ $item->id }}" method="POST">
                    @method('delete')
                    @csrf
                    <a href="/cast/{{ $item->id }}" class="btn-success btn-sm">Detail</a>
                    <a href="/cast/{{ $item->id }}/edit" class="btn-info btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    
                </form>
            </td>
          </tr>
      @empty
         <tr>
            <td>Data Cast Kosong</td>
         </tr>
      @endforelse
    </tbody>
</table>

@endsection