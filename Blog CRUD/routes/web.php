<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [HomeController::class, 'home']); //route utama home
Route::get('/register', [AuthController::class, 'register']); //route link form sign up di home


Route::post('/post_register', [AuthController::class, 'send']); //route ketika kita klin sign up maka akan di arahkan ke halaman selanjutnya

Route::get('/table', function(){
    return view('table');        //untuk route data table
});

Route::get('/data-table', function(){
    return view('halamantable');        //untuk route halaman data table
});

//CRUD CAST

//Create Data
//Route untuk mengarah ke form tambah cast
Route::get('/cast/create', [CastController::class, 'create']); 
//Route untuk menyimpan data inputan ke table Cast Database
Route::post('/cast', [CastController::class, 'store']);

//Read Cast
//Route untuk tampil semua data di table Cast DB
Route::get('/cast', [CastController::class, 'index']);
//Route untuk detail data bye id
Route::get('/cast/{id}', [CastController::class, 'show']); //{id} sebagai parameter

//Updata Cast
//Route untuk mengarah ke form Edit Cast
Route::get('/cast/{id}/edit', [CastController::class, 'edit']); 
//Route unutk update data by ID table cast DB
Route::put('/cast/{id}', [CastController::class, 'update']);

//Delete Cast
//Route untuk delete cas by id nya
Route::delete('/cast/{id}', [CastController::class, 'destroy']);
