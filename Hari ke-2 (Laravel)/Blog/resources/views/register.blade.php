<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/post_register" method="POST"> 
    @csrf
        <label>First name:</label><br>
        <input type="text" name="frist_name" placeholder="First name"><br><br>
        <label>Last name:</label><br>
        <input type="text" name="last_name" placeholder="Last name"><br><br>
        <label>Gender:</label><br>
        <input type="radio" name="gender" value="male">Male<br>
        <input type="radio" name="gender" value="female">Female<br>
        <input type="radio" name="gender" value="other">Other<br>
        <label>Nationality:</label><br>
        <select name="nationality" id="">
            <option value="indonesia">Indonesia</option>
            <option value="inggris">Inggris</option>
            <option value="other">Other</option>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type="checkbox" name="language_spoken">Bahasa Indonesia<br>
        <input type="checkbox" name="language_spoken">English<br>
        <input type="checkbox" name="language_spoken">Singapore<br>
        <input type="checkbox" name="language_spoken">Other<br><br>
        <label>Bio:</label><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" name="submit" value="Sign Up">
    </form>
</body>
</html>