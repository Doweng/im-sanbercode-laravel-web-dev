<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function send(Request $request)  //function send untuk route mengirim ke halaman selanjutnya
    {
        $fristName = $request['frist_name'];     //untuk mengirim ke view welcome
        $lastName = $request['last_name'];       //last_name dari register.blade.php input type name NYA

        return view('welcome', ['fristName' => $fristName, 'lastName' => $lastName]);
    }

    public function register(){
        return view('register');  //function untuk method yg d route get register
    }
}
